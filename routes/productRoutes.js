const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Create product
router.post(
  "/add",
  auth.verify,
  auth.verifyAdmin,
  productController.createProduct
);

// Get all product
router.get("/all", productController.getAll);

// Get all active product
router.get("/all/active", productController.getActive);

// get specific product by id
router.get("/:productId", productController.getProductById);

// get specific product by name
router.post("/search", productController.getProductByName);

//[ACTIVITY] Search Courses By Price Range
router.post("/searchByPrice", productController.searchProductByPriceRange);

// update product information using params id
router.put(
  "/:productId",
  auth.verify,
  auth.verifyAdmin,
  productController.updateProduct
);

// Archive/deactivate product
router.put(
  "/:productId/archive",
  auth.verify,
  auth.verifyAdmin,
  productController.archiveProduct
);

// Activate product
router.put(
  "/:productId/activate",
  auth.verify,
  auth.verifyAdmin,
  productController.activateProduct
);

module.exports = router;
