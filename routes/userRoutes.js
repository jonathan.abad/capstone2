const express = require("express");

const userController = require("../controllers/userController.js");
const auth = require("../auth.js");
const Cart = require("../models/Cart.js");

const router = express.Router();

// Route for checking if user email already exist.
router.post("/checkEmail", (request, response) => {
  userController
    .checkEmailExists(request.body)
    .then((resultFromController) => response.send(resultFromController));
});

// Register user
router.post("/register", (request, response) => {
  userController.registerUser(request.body).then((result) => {
    response.send(result);
  });
});

// Login user
router.post("/login", userController.loginUser);

// retrieve user details
router.get("/details", auth.verify, userController.getDetails);

router.put("/reset-password", auth.verify, userController.resetPassword);

// Update user profile route
router.put("/profile", auth.verify, userController.updateProfile);

// Get all Orders
router.get("/allOrder", auth.verify, auth.verifyAdmin, (request, response) => {
  userController.getAllOrders(request, response);
});

// retrieve authenticated user's order
router.get("/orders", auth.verify, (request, response) => {
  userController.getUserOrders(request, response);
});

// Set user as admin (admin only)
router.put(
  "/:id/setAsAdmin",
  auth.verify,
  auth.verifyAdmin,
  (request, response) => {
    userController.setAsAdmin(request, response);
  }
);

// add/edit to cart for authenticated user
router.post("/cart/addToCart", auth.verify, (request, response) => {
  userController.addToCart(request, response);
});

// remove product in cart
router.delete("/cart/:productId", auth.verify, (request, response) => {
  userController.removeFromCart(request, response);
});

// place order/ checkout of the cart of user
router.post("/place-order", auth.verify, (request, response) => {
  userController.placeOrder(request, response);
});

// View user's cart
router.get("/cart/view", auth.verify, async (req, res) => {
  try {
    const userId = req.user.id;
    const cart = await Cart.findOne({ userId });

    if (!cart) {
      return res.status(404).json({
        message: "Cart not found",
      });
    }

    return res.json(cart);
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      error: "Internal server error",
    });
  }
});

module.exports = router;
