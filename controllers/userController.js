const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const Order = require("../models/Order");

module.exports.checkEmailExists = (user_body) => {
  return User.find({ email: user_body.email }).then((result) => {
    // The "find" metho
    // returns a record if a match is found.
    if (result.length > 0) {
      return true; //"Duplicate email found"
    } else {
      return false;
    }
  });
};

// Function to reset the password
module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json(true);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// register user
module.exports.registerUser = (user_body) => {
  let {
    name,
    email,
    password,
    mobileNumber,
    address: [{ streetNumber, barangay, municipality, province }],
  } = user_body;
  const hashedPassword = bcrypt.hashSync(password, 10);

  let newUser = new User({
    name,
    email,
    password: hashedPassword,
    mobileNumber,
    address: [{ streetNumber, barangay, municipality, province }],
  });
  return newUser
    .save()
    .then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    })
    .catch((error) => error);
};

// Login
module.exports.loginUser = (request, response) => {
  return User.findOne({ email: request.body.email })
    .then((result) => {
      // User does not exist.
      console.log(result);
      if (result == null) {
        return response.send(false);
      } else {
        // Created the isPasswordCorrect variable to return the result of comparing the login form password and the database password.
        // compareSync() method is used to compare the non-encrypted password from the login form to the encrypted password from the database.
        // will return either "true" or "false"
        const isPasswordCorrect = bcrypt.compareSync(
          request.body.password,
          result.password
        );

        if (isPasswordCorrect) {
          // Generate an access token
          // Uses the "createAccessToken" method defined in the "auth.js" file.
          // Returning an object back to the frontend application is a common practice to encure that information is properly labled and real world examples normally return more complex information represented by objects.
          return response.send({ access: auth.createAccessToken(result) });
        } else {
          // If password is incorrect.
          return response.send(false);
        }
      }
    })
    .catch((err) => response.send(err));
};

// get all orders
module.exports.getAllOrders = (request, response) => {
  return Order.find({})
    .then((orders) => {
      return response.send(orders);
    })
    .catch((error) => response.send(erro));
};

// get orders of users that is authenticated
module.exports.getUserOrders = async (request, response) => {
  const userId = request.user.id;

  try {
    const orders = await Order.find({ userId });

    if (!orders || orders.length === 0) {
      return response.json({ message: "No orders found for the user" });
    }
    response.json({
      message: "User's orders retrieved successfully",
      data: orders,
    });
  } catch (error) {
    console.error(error);
    response.status(500).json({ error: "Internal server error" });
  }
};

// get details of the user
module.exports.getDetails = (request, response) => {
  return User.findById(request.user.id)
    .then((result) => {
      result.password = "";
      return response.send(result);
    })
    .catch((error) => response.send(error));
};

// set user as admin
module.exports.setAsAdmin = (request, response) => {
  let set_asAdmin = {
    isAdmin: true,
  };
  return User.findByIdAndUpdate(request.params.id, set_asAdmin)
    .then((result, error) => {
      if (error) {
        return response.send({
          message: error.message,
        });
      }
      return response.send({
        message: "User has been set to admin",
      });
    })
    .catch((error) => console.log(error));
};

// Update or add to cart
module.exports.addToCart = async (request, response) => {
  if (request.user.isAdmin) {
    return response.status(403).json({
      message: "Action Forbidden",
    });
  }
  const userId = request.user.id;
  const productsToUpdate = request.body;

  try {
    let cart = await Cart.findOne({ userId });

    if (!cart) {
      cart = new Cart({
        userId,
        products: [],
        totalAmount: 0,
      });
    }

    for (const productInfo of productsToUpdate) {
      const { productId, quantity } = productInfo;
      const product = await Product.findById(productId);

      if (!product) {
        return response.status(404).json({
          error: `Product with ID ${productId} not found`,
        });
      }

      const existingProduct = cart.products.find(
        (p) => p.productId === productId
      );

      if (existingProduct) {
        // Update existing product's quantity and total
        cart.totalAmount -= existingProduct.total;
        existingProduct.quantity = quantity;
        existingProduct.total = product.price * quantity;
        cart.totalAmount += existingProduct.total;
      } else {
        // Add new product to cart
        const total = product.price * quantity;
        cart.products.push({
          productId,
          productName: product.name,
          quantity,
          total,
          productImageUrl: product.imageUrl,
          productDescription: product.description,
        });
        cart.totalAmount += total;
      }
    }

    await cart.save();

    return response.json({
      message: "Cart updated successfully",
      userCart: cart,
      totalPerProduct: cart.products.map((product) => ({
        productId: product.productId,
        total: product.total,
      })),
      totalPrice: cart.totalAmount,
    });
  } catch (error) {
    console.error(error);
    response.status(500).json({
      error: "Internal server error",
    });
  }
};

// Remove product from Cart
module.exports.removeFromCart = async (request, response) => {
  const userId = request.user.id;
  const { productId } = request.params;

  try {
    const cart = await Cart.findOne({ userId });

    if (!cart) {
      return response.send({
        message: "Cart not found",
      });
    }
    const removedProduct = cart.products.find((p) => p.productId === productId);
    if (!removedProduct) {
      return response.send({
        error: `Product with ID ${productId} not found in the cart`,
      });
    }
    cart.totalAmount -= removedProduct.total;
    cart.products = cart.products.filter((p) => p.productId !== productId);

    await cart.save();

    return response.send({
      message: "Product removed from cart successfully",
    });
  } catch (error) {
    console.error(error);
    response.status(500).json({
      error: "Internal server error",
    });
  }
};

// Place order when user is ready to checkout, the cart of the user will be cleared after checkout and saved in order model
module.exports.placeOrder = async (request, response) => {
  const userId = request.user.id;

  try {
    // Find the user's cart
    const cart = await Cart.findOne({ userId });
    if (!cart || cart.products.length === 0) {
      return response.status(400).json({ error: "Cart is empty" });
    }

    // Fetch the user's name from the database
    const user = await User.findById(userId);
    if (!user) {
      return response.status(404).json({ error: "User not found" });
    }

    // Create an order from the cart
    const order = new Order({
      userId,
      userName: user.name, // Include the user's name
      products: cart.products,
      totalAmount: cart.totalAmount,
    });

    // Clear the user's cart
    cart.products = [];
    cart.totalAmount = 0;
    await cart.save();

    // Save the order
    await order.save();
    console.log(order);
    response
      .status(201)
      .json({ message: "Order placed successfully", data: order });
  } catch (error) {
    console.error(error);
    response.status(500).json({ error: "Internal server error" });
  }
};

module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const {
      name,
      email,
      password,
      mobileNumber,
      address: [{ streetNumber, barangay, municipality, province }],
    } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      {
        name,
        email,
        password,
        mobileNumber,
        address: [{ streetNumber, barangay, municipality, province }],
      },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to update profile" });
  }
};
