const Product = require("../models/Product.js");

//create new product
module.exports.createProduct = (request, response) => {
  let new_product = new Product({
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
    imageUrl: request.body.imageUrl,
  });
  return new_product
    .save()
    .then((createdProduct, error) => {
      if (error) {
        return response.send(false);
      } else {
        return response.send(true);
      }
    })
    .catch((error) => response.send(error));
};

// get all prodcuts
module.exports.getAll = (request, response) => {
  return Product.find({})
    .then((result) => {
      return response.send(result);
    })
    .catch((error) => response.send(error));
};
// get active products
module.exports.getActive = (request, response) => {
  return Product.find({ isActive: true })
    .then((result) => {
      return response.send(result);
    })
    .catch((error) => response.send(error));
};

// get product by id
module.exports.getProductById = (request, response) => {
  return Product.findById(request.params.productId)
    .then((result) => {
      console.log(result);
      return response.send(result);
    })
    .catch((err) => response.send(err));
};

// get product by name
module.exports.getProductByName = async (request, response) => {
  try {
    const { productName } = request.body;

    const products = await Product.find({
      name: { $regex: productName, $options: "i" },
    });
    response.json(products);
  } catch (error) {
    console.error(error);
    response.status(500).json({ error: "Internal Server Error" });
  }
};

// update product
module.exports.updateProduct = (request, response) => {
  let updated_product = {
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
  };
  return Product.findByIdAndUpdate(request.params.productId, updated_product)
    .then((product, error) => {
      if (error) {
        return response.send(false);
      } else {
        return response.send(true);
      }
    })
    .catch((error) => response.send(error));
};

// archive/deactivate product
module.exports.archiveProduct = (request, response) => {
  let archive_product = {
    isActive: false,
  };
  return Product.findByIdAndUpdate(request.params.productId, archive_product)
    .then((product, error) => {
      if (error) {
        return response.send(false);
      } else {
        return response.send(true);
      }
    })
    .catch((error) => response.send(error));
};

// activate product
module.exports.activateProduct = (request, response) => {
  let activate_product = {
    isActive: true,
  };
  return Product.findByIdAndUpdate(request.params.productId, activate_product)
    .then((product, error) => {
      if (error) {
        return response.send(false);
      } else {
        return response.send(true);
      }
    })
    .catch((err) => res.send(err));
};

module.exports.searchProductByPriceRange = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    // Find courses within the price range
    const products = await Product.find({
      price: { $gte: minPrice, $lte: maxPrice },
    });

    res.status(200).json({ products });
  } catch (error) {
    res
      .status(500)
      .json({ error: "An error occurred while searching for courses" });
  }
};
